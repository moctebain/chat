'use strict';
var socket = io(),
    Handlers;

function FormHandlers(){
  this.joinChat = function(nickname){
    this.sendEvent('join', nickname);  
  }

  this.sendMessage = function(form){
    var msg = form.find('[name="message"]').val();
    if (msg !== ''){
      this.sendEvent('new-message', msg);    
      this.clearForm(form);
    }
  }

  this.sendEvent = function(name, info){
    socket.emit(name, info)
  }
}

FormHandlers.prototype.clearForm = function(form){
  form.find('input[type="text"], textarea').val('')
}

FormHandlers.prototype.render = function(data){
  var html = data.map(function(elem, index) {
        return('<div><strong>' + elem.author +'</strong>: <span>' + elem.text + '</span>');
      }).join(' ');
  $('#messages').html(html).scrollTop($('#messages').prop('scrollHeight'));
}

FormHandlers.prototype.removeHTML = function(str){
  return $('<div>' + str + '</div>').text()
}

FormHandlers.prototype.removeFormErrors = function(form){
  form.removeClass('has-error').find('.error').remove();
}

FormHandlers.prototype.showFormErrors = function(form, msg){
  form.addClass('has-error').append('<b class="error">' + msg + '</b>')
}

FormHandlers.prototype.validateLogin = function(cb){
  var nickname = this.removeHTML($(".nickname").val()), form = $('.form-login .form-group');
  this.removeFormErrors(form);
  
  if((nickname !== "") && (nickname.length > 2)){    
    cb.call(this, nickname);
    this.clearForm(form);
  } else {
    this.showFormErrors(form, 'It has to be a valid nickname!');
  }
}

FormHandlers.prototype.validateRoom = function(room_name, cb, ev){
  var room = this.removeHTML(room_name), form = $('.form-room .form-group');
  this.removeFormErrors(form);
  
  if((room !== "") && (room.length > 2)){    
    cb.apply(this, [ev, room]);
    $('#create-room').modal('hide');
    this.clearForm(form);
  } else {
    this.showFormErrors(form, 'It has to be a valid room name!');
  }
}

Handlers = new FormHandlers();

// Form login
$('.form-login').on('submit', function(e){
  e.preventDefault();
  Handlers.validateLogin(Handlers.joinChat);
});

// Form room
$('.form-room').on('submit', function(e){
  e.preventDefault();
  Handlers.validateRoom($('[name="room"]').val(), Handlers.sendEvent, 'create-room')   
});

// Join room
$('.rooms').on('click', '.join-room', function(){
  var room_id = $(this).parents('div').attr('data-room-id');
  $('.chat').attr('data-inroom-id', room_id)
  Handlers.sendEvent('join-room', room_id)
});

// Remove room
$('.rooms').on('click', '.remove-room', function(){
  Handlers.sendEvent('remove-room', $(this).parents('div').attr('data-room-id'))
});

// Leave room
$('.joined-room').on('click', '.leave-room', function(){  
  Handlers.sendEvent('leave-room', {
    userID: $('[data-user-id]').attr('data-user-id'),
    roomID: $('.chat').attr('data-inroom-id')
  });
});

// Form message
$('.form-message').on('submit', function(e){
  e.preventDefault();
  Handlers.sendMessage($('.form-message'));
});

$('.logout').on('click', function(){
  var res = confirm('Are you sure?');
  if(res){ 
    Handlers.sendEvent('logout', {
      userID: $('[data-user-id]').attr('data-user-id')
    })
  }
})

// Show dashboard
socket.on('show-dashboard', function(info){
    $('.chat').show().attr('data-user-id', info.id);
    $('.user-logged').html('You are logged-in as: <strong class="sys-msg">"' + info.name + '"</strong>');
    $('.welcome').hide();  
});

// Show room
socket.on('show-joined-room', function(room){
  $('.joined-room').empty().html('<strong>' + room.name + '</strong> | <a href="javascript:void(0);" class="leave-room" data-room-id="' + room.id + '">Leave room</a>');
});

// Hide room
socket.on('hide-joined-room', function(){
  $('#messages, .joined-room').empty();
  $('[name="message"]').val('')
  $('.chat').removeAttr('data-inroom-id');
});

// Show messages
socket.on('show-messages', function(data) {    
  Handlers.render(data);
});

// Status messages
socket.on('update', function(status){
  $('.status-msgs').append('<div>' + status + '</div>').scrollTop($('.status-msgs').prop('scrollHeight'));
});

// Alert
socket.on('alert', function(alert){
  $('.alert' + alert.el).removeClass('alert-success alert-danger hide').addClass(alert.class_name).find('.msg').html(alert.msg);
});

// Update joined people
socket.on('update-people', function(people){  
  $('.people').empty();    
  $.each(people, function(index){
    $('.people').append('<div>' + people[index].name + '</div>')  
  });  
});

// Rooms list
socket.on('rooms-list', function(data){   
  if(Object.keys(data.rooms).length > 0){
    $('.rooms').empty();    
    $.each(data.rooms, function(id, room){
      var room_opts = '';
      if($('.chat').attr('data-user-id') === id){
        room_opts = ' | <a href="javascript:void(0);" class="remove-room">Delete</a>'
      }
      $('.rooms').append('<div data-room-id=' + id + '>' + room.name + ' <a href="javascript:void(0);" class="join-room">Join</a>' + room_opts + '</div>')  
    });
  } else {
    $('.rooms').html('There isn\'t available rooms yet');
  }
});

// Show dashboard
socket.on('logout', function(id){
    window.location.reload();
});