'use strict';
var express = require('express'),
    app = express(),
    server = require('http').Server(app),
    socket = require('socket.io')(server),
    _ = require('underscore'),
    striptags = require('striptags'),
    Room = require('./room'),    
    people = {},
    rooms = {},
    messages = {};

app.use(express.static('public'));

function getRoomsList(){
  return {
    rooms: rooms
  }
}

socket.on('connection', function(client){
  console.log('User connected');

  // Join
  client.on('join', function(nickname){            
    if(_.where(people, {name: nickname}).length === 0){      
      people[client.id] = {
        id: client.id,
        name: striptags(nickname),
        room: null,
        inroom: null
      }
      socket.sockets.emit('update', '"<strong>' + nickname + '</strong>"" have joined the server.');      
      client.emit('show-dashboard', {
        id: client.id,
        name: nickname
      });
      client.emit('rooms-list', getRoomsList())
    } else {

      client.emit('alert', {
        el: '.login-msg',
        class_name: 'alert-danger',
        msg: 'Choose another nickname. This is unavailable.'
      })
    }
  }); 

  // Create room
  client.on('create-room', function(name){
    var id = client.id, room = null;
    if(_.where(rooms, {name: name}).length === 0){
      if(people[client.id].room === null){
        room = new Room(id, name, client.id);
        rooms[id] = room;
        socket.sockets.emit('rooms-list', getRoomsList());
        people[client.id].room = id;
        client.emit('update', '<strong>Congratulations!</strong> You have created a room!</strong>');
      } else {
        client.emit('update', '<strong class="sys-msg">You already have created a room!</strong>');
      }
    } else {
      client.emit('update', '<strong class="sys-msg">Choose another name for the room. This is unavailable.</strong>');
    }
  });

  // Join room
  client.on('join-room', function(id){
    var room = rooms[id];
    if(people[client.id].inroom === null){
      room.addUser(client.id);  
      people[client.id].inroom = room.id;    
      client.emit('update', 'Welcome to "<strong>' + room.name + '</strong>" room!');
      client.emit('show-joined-room', {
        id: room.id,
        name: room.name
      });
      client.room = room.id;
    } else {
      client.emit('update', '<strong class="sys-msg">You already sign in a room!</strong>');
    } 
  });

  // Remove room
  client.on('remove-room', function(id){
    var room = rooms[id]
    if(people[client.id].room !== null){      
      delete rooms[client.id];
      delete messages[client.id];      
      socket.sockets.emit('rooms-list', getRoomsList());
      client.emit('update', 'You have deleted "<strong>' + room.name + '</strong>" successfully!');

      room.people.forEach(function(socketID){    
        people[socketID].room = null;      
        people[socketID].inroom = null;
        socket.to(socketID).room = null;      
        socket.to(socketID).emit('hide-joined-room');  
      });

    } else {
      client.emit('update', '<strong>You can not delete this room!</strong>');
    }
  });

  // Leave room
  client.on('leave-room', function(user){
    var room = rooms[user.roomID];
    if(people[client.id].inroom !== null){
      room.removeUser(user.userID);
      people[client.id].inroom = null;      
      client.emit('update', 'You have left "<strong>' + room.name + '</strong>" successfully!');
      client.emit('hide-joined-room');
    }
  });

  // New message
  client.on('new-message', function(msg){
    var room;

    if(people[client.id].inroom !== null){
      room = rooms[people[client.id].inroom];
      if(typeof messages[people[client.id].inroom] === 'undefined'){
        messages[people[client.id].inroom] = [];
      }
      messages[people[client.id].inroom].push({
        author: people[client.id].name,
        text: striptags(msg)
      });

      // Emit to all connected "clients" / sockets
      room.people.forEach(function(socketID){        
        socket.to(socketID).emit('show-messages', messages[people[client.id].inroom]);  
      });      
    } else {
      client.emit('update', '<strong class="sys-msg">You have to join to a room to post a new message!</strong>');
    }
  });

  // Logout
  client.on('logout', function(user_info){
    var room,
        user_id = user_info.userID,
        room_id = client.room,
        room_people;
    
    if(typeof room_id !== 'undefined'){
      room = rooms[room_id];
      room_people = room.people;
      room.removeUser(user_id);

      // Emit to all connected "clients" / sockets
      room_people.forEach(function(socketID){    
        people[socketID].room = null;
        people[socketID].inroom = null;
        socket.to(socketID).emit('hide-joined-room');  
      });
    }

    if(messages[user_id]){      
      delete messages[user_id];
    }

    client.emit('logout');    
    client.disconnect();
  });

  // Disconnect
  client.on('disconnect', function(){
    client._events.logout({
      userID: client.id
    });
    delete people[client.id];
    delete rooms[client.id];     
    socket.sockets.emit('rooms-list', getRoomsList());
  });
  
});

server.listen(3000, function(){
	console.log('Listening...');
});