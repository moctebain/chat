module.exports = function(grunt){

	grunt.initConfig({	

		// http://eslint.org/docs/rules/
		eslint: {
			nodeFiles: {
				src: ['app.js', 'controllers/**/*.js', 'config/**/*.js'],
				options: {
					configFile: 'eslint.json'
				}
			},
			browserFiles: {
				src: ['public/assets/js/**/*.js', '!public/assets/js/vendor/*.js'],
				options: {
					configFile: 'eslint.json'
				}
			}
		},

		watch: {
			scripts: {
				files: ['**/*.html', 'app.js', 'views/**/*.ejs', 'locales/**/*.json', 'public/assets/css/scss/**/*', 'public/assets/js/**/*'],
				tasks: ['lintNode', 'lintBrowser'],
				options: {
					livereload: {
						port: 35800
					}					
				}
			}			
		}	

	});

	// Load all grunt tasks
	grunt.loadNpmTasks('gruntify-eslint');
	grunt.loadNpmTasks('grunt-contrib-watch');	

	// Run Default task(s).
	grunt.registerTask('lintNode', ['eslint:nodeFiles']);
	grunt.registerTask('lintBrowser', ['eslint:browserFiles']);	
	grunt.registerTask('default', ['lintNode', 'lintBrowser','watch']);
}