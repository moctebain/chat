'use strict';
function Room(id, name, owner){
  this.id = id;
  this.name = name;
  this.owner = owner;
  this.people = [];
  this.status = 'available';
}

Room.prototype.addUser = function(userID){
  if(this.status === 'available'){
    this.people.push(userID);
  }
}

Room.prototype.removeUser = function(userID){
  this.people.splice([this.people.indexOf(userID)], 1);
}

module.exports = Room;