'use strict';
// Documentation: 
// http://jasmine.github.io/edge/introduction.html
// https://www.codementor.io/javascript/tutorial/javascript-testing-framework-comparison-jasmine-vs-mocha
var Room = require('../room');

describe('Examples for room object', function() {
	var room = new Room('ID', 'Room name', 'Owner');	

	it('Add new item to an array', function(done){
		room.addUser('userid');	
		expect(room.people.length).toBeGreaterThan(0);
		done();
	});

	it('Check that id exists in array', function(done){
		expect(room.people.indexOf('userid')).toBeGreaterThan(-1);
		done();
	});

	it('Removes an item from array', function(done){
		var people = room.people.length;
		room.removeUser('userid')
		expect(room.people.length).toBeLessThan(people);
		done();
	})
});